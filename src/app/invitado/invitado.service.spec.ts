import { TestBed, inject } from '@angular/core/testing';

import { InvitadoService } from './invitado.service';

describe('InvitadoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InvitadoService]
    });
  });

  it('should be created', inject([InvitadoService], (service: InvitadoService) => {
    expect(service).toBeTruthy();
  }));
});
