import { NgModule } 				from '@angular/core';
import { RouterModule, Routes } 	from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { InvitadoComponent }		from './invitado/invitado.component';

import { InicioComponent } 			from './inicio/inicio.component';
import { ContactoComponent } 		from './contacto/contacto.component';
import { QuienessomosComponent } 	from './quienessomos/quienessomos.component';


const appRoutes: Routes = [
	
	{ path: 'invitado', component: InvitadoComponent},
	{ path: 'contacto', component: ContactoComponent},
	{ path: 'quienessomos', component: QuienessomosComponent},
	{ path: '**', component: InicioComponent}
];

@NgModule({
	imports: [
		RouterModule.forRoot(appRoutes)
	],
	exports: [
		RouterModule
	]


})
export class AppRoutingModule {};
export const appRoutingProvides: any[]=[];
export const routing: ModuleWithProviders=RouterModule.forRoot(appRoutes);