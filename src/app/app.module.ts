import { BrowserModule }        from '@angular/platform-browser';
import { NgModule }             from '@angular/core';

import { RouterModule }     from '@angular/router';
import { AppComponent }     from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { routing,appRoutingProvides} from './app-routing.module';

import { InvitadoComponent } from './invitado/invitado.component';

import { InicioComponent } from './inicio/inicio.component';
import { ContactoComponent } from './contacto/contacto.component';
import { QuienessomosComponent } from './quienessomos/quienessomos.component';




@NgModule({
  declarations: [
    AppComponent,
    InvitadoComponent,
    InicioComponent,
    ContactoComponent,
    QuienessomosComponent,
    
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
  ],
  providers: [
    appRoutingProvides
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }


